<?php
App::uses('AppController', 'Controller');
/**
 * ChallengeAnswers Controller
 *
 * @property ChallengeAnswer $ChallengeAnswer
 * @property PaginatorComponent $Paginator
 */
class ChallengeAnswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ChallengeAnswer->recursive = 0;
		$this->set('challengeAnswers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ChallengeAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid challenge answer'));
		}
		$options = array('conditions' => array('ChallengeAnswer.' . $this->ChallengeAnswer->primaryKey => $id));
		$this->set('challengeAnswer', $this->ChallengeAnswer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ChallengeAnswer->create();
			if ($this->ChallengeAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge answer could not be saved. Please, try again.'));
			}
		}
		$challenges = $this->ChallengeAnswer->Challenge->find('list');
		$challengeQuestions = $this->ChallengeAnswer->ChallengeQuestion->find('list');
		$this->set(compact('challenges', 'challengeQuestions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChallengeAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid challenge answer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChallengeAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge answer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChallengeAnswer.' . $this->ChallengeAnswer->primaryKey => $id));
			$this->request->data = $this->ChallengeAnswer->find('first', $options);
		}
		$challenges = $this->ChallengeAnswer->Challenge->find('list');
		$challengeQuestions = $this->ChallengeAnswer->ChallengeQuestion->find('list');
		$this->set(compact('challenges', 'challengeQuestions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChallengeAnswer->id = $id;
		if (!$this->ChallengeAnswer->exists()) {
			throw new NotFoundException(__('Invalid challenge answer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ChallengeAnswer->delete()) {
			$this->Session->setFlash(__('The challenge answer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The challenge answer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
