<?php
App::uses('AppController', 'Controller');
/**
 * ChallengeQuestions Controller
 *
 * @property ChallengeQuestion $ChallengeQuestion
 * @property PaginatorComponent $Paginator
 */
class ChallengeQuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ChallengeQuestion->recursive = 0;
		$this->set('challengeQuestions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ChallengeQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid challenge question'));
		}
		$options = array('conditions' => array('ChallengeQuestion.' . $this->ChallengeQuestion->primaryKey => $id));
		$this->set('challengeQuestion', $this->ChallengeQuestion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ChallengeQuestion->create();
			if ($this->ChallengeQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge question could not be saved. Please, try again.'));
			}
		}
		$challenges = $this->ChallengeQuestion->Challenge->find('list');
		//$levels = $this->ChallengeQuestion->Level->find('list');
		$this->set(compact('challenges', 'levels'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChallengeQuestion->exists($id)) {
			throw new NotFoundException(__('Invalid challenge question'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChallengeQuestion->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge question has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge question could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChallengeQuestion.' . $this->ChallengeQuestion->primaryKey => $id));
			$this->request->data = $this->ChallengeQuestion->find('first', $options);
		}
		$challenges = $this->ChallengeQuestion->Challenge->find('list');
		$levels = $this->ChallengeQuestion->Level->find('list');
		$this->set(compact('challenges', 'levels'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChallengeQuestion->id = $id;
		if (!$this->ChallengeQuestion->exists()) {
			throw new NotFoundException(__('Invalid challenge question'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ChallengeQuestion->delete()) {
			$this->Session->setFlash(__('The challenge question has been deleted.'));
		} else {
			$this->Session->setFlash(__('The challenge question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
