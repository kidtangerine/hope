<?php
App::uses('AppController', 'Controller');
/**
 * ChallengeDescriptions Controller
 *
 * @property ChallengeDescription $ChallengeDescription
 * @property PaginatorComponent $Paginator
 */
class ChallengeDescriptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ChallengeDescription->recursive = 0;
		$this->set('challengeDescriptions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ChallengeDescription->exists($id)) {
			throw new NotFoundException(__('Invalid challenge description'));
		}
		$options = array('conditions' => array('ChallengeDescription.' . $this->ChallengeDescription->primaryKey => $id));
		$this->set('challengeDescription', $this->ChallengeDescription->find('first', $options));
                //debug($this->ChallengeDescription->find('first', $options));
               $this->layout = 'loggedin';
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ChallengeDescription->create();
			if ($this->ChallengeDescription->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge description has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge description could not be saved. Please, try again.'));
			}
		}
		$challenges = $this->ChallengeDescription->Challenge->find('list');
		$this->set(compact('challenges'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ChallengeDescription->exists($id)) {
			throw new NotFoundException(__('Invalid challenge description'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ChallengeDescription->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge description has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge description could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ChallengeDescription.' . $this->ChallengeDescription->primaryKey => $id));
			$this->request->data = $this->ChallengeDescription->find('first', $options);
		}
		$challenges = $this->ChallengeDescription->Challenge->find('list');
		$this->set(compact('challenges'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ChallengeDescription->id = $id;
		if (!$this->ChallengeDescription->exists()) {
			throw new NotFoundException(__('Invalid challenge description'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->ChallengeDescription->delete()) {
			$this->Session->setFlash(__('The challenge description has been deleted.'));
		} else {
			$this->Session->setFlash(__('The challenge description could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
