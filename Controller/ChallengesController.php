<?php
App::uses('AppController', 'Controller');
/**
 * Challenges Controller
 *
 * @property Challenge $Challenge
 * @property PaginatorComponent $Paginator
 */
class ChallengesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Challenge->recursive = 0;
		$this->set('challenges', $this->Paginator->paginate());
	}
/**
 * Description method
 *
 * @return void
 */
        public function description() {
                
            
        }
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Challenge->exists($id)) {
			throw new NotFoundException(__('Invalid challenge'));
		}
		$options = array('conditions' => array('Challenge.' . $this->Challenge->primaryKey => $id));
                //debug($this->Challenge->find('first', $options));
		$this->set('challenge', $this->Challenge->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Challenge->create();
			if ($this->Challenge->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Challenge->exists($id)) {
			throw new NotFoundException(__('Invalid challenge'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Challenge->save($this->request->data)) {
				$this->Session->setFlash(__('The challenge has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The challenge could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Challenge.' . $this->Challenge->primaryKey => $id));
			$this->request->data = $this->Challenge->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Challenge->id = $id;
		if (!$this->Challenge->exists()) {
			throw new NotFoundException(__('Invalid challenge'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Challenge->delete()) {
			$this->Session->setFlash(__('The challenge has been deleted.'));
		} else {
			$this->Session->setFlash(__('The challenge could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
