<div class="challenges view">
<h2><?php echo __('Challenge'); ?></h2>
	<dl>
		
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($challenge['Challenge']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($challenge['Challenge']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level'); ?></dt>
		<dd>
			<?php echo h($challenge['Challenge']['level']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Challenge'), array('action' => 'edit', $challenge['Challenge']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Challenge'), array('action' => 'delete', $challenge['Challenge']['id']), null, __('Are you sure you want to delete # %s?', $challenge['Challenge']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('controller' => 'challenge_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('controller' => 'challenge_questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('controller' => 'challenge_questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Challenge Answers'); ?></h3>
	<?php if (!empty($challenge['ChallengeAnswer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Challenge Id'); ?></th>
		<th><?php echo __('Challenge Question Id'); ?></th>
		<th><?php echo __('Response'); ?></th>
		<th><?php echo __('Correct'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($challenge['ChallengeAnswer'] as $challengeAnswer): ?>
		<tr>
			<td><?php echo $challengeAnswer['id']; ?></td>
			<td><?php echo $challengeAnswer['challenge_id']; ?></td>
			<td><?php echo $challengeAnswer['challenge_question_id']; ?></td>
			<td><?php echo $challengeAnswer['response']; ?></td>
			<td><?php echo $challengeAnswer['correct']; ?></td>
			<td><?php echo $challengeAnswer['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'challenge_answers', 'action' => 'view', $challengeAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'challenge_answers', 'action' => 'edit', $challengeAnswer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'challenge_answers', 'action' => 'delete', $challengeAnswer['id']), null, __('Are you sure you want to delete # %s?', $challengeAnswer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Challenge Questions'); ?></h3>
	<?php if (!empty($challenge['ChallengeQuestion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Challenge Id'); ?></th>
		<th><?php echo __('Question'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Level Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($challenge['ChallengeQuestion'] as $challengeQuestion): ?>
		<tr>
			<td><?php echo $challengeQuestion['id']; ?></td>
			<td><?php echo $challengeQuestion['challenge_id']; ?></td>
			<td><?php echo $challengeQuestion['question']; ?></td>
			<td><?php echo $challengeQuestion['type']; ?></td>
			<td><?php echo $challengeQuestion['level_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'challenge_questions', 'action' => 'view', $challengeQuestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'challenge_questions', 'action' => 'edit', $challengeQuestion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'challenge_questions', 'action' => 'delete', $challengeQuestion['id']), null, __('Are you sure you want to delete # %s?', $challengeQuestion['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Challenge Question'), array('controller' => 'challenge_questions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
