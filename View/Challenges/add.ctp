
<?php echo $this->Form->create('Challenge'); ?>
	<fieldset>
		<legend><?php echo __('Ad1d Challenge'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('level');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Challenges'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('controller' => 'challenge_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('controller' => 'challenge_questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('controller' => 'challenge_questions', 'action' => 'add')); ?> </li>
	</ul>

