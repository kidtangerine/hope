<div class="challengeAnswers view">
<h2><?php echo __('Challenge Answer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($challengeAnswer['ChallengeAnswer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Challenge'); ?></dt>
		<dd>
			<?php echo $this->Html->link($challengeAnswer['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $challengeAnswer['Challenge']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Challenge Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($challengeAnswer['ChallengeQuestion']['question'], array('controller' => 'challenge_questions', 'action' => 'view', $challengeAnswer['ChallengeQuestion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response'); ?></dt>
		<dd>
			<?php echo h($challengeAnswer['ChallengeAnswer']['response']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Correct'); ?></dt>
		<dd>
			<?php echo h($challengeAnswer['ChallengeAnswer']['correct']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($challengeAnswer['ChallengeAnswer']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Challenge Answer'), array('action' => 'edit', $challengeAnswer['ChallengeAnswer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Challenge Answer'), array('action' => 'delete', $challengeAnswer['ChallengeAnswer']['id']), null, __('Are you sure you want to delete # %s?', $challengeAnswer['ChallengeAnswer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('controller' => 'challenge_questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('controller' => 'challenge_questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
