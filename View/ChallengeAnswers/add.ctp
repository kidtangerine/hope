<div class="challengeAnswers form">
<?php echo $this->Form->create('ChallengeAnswer'); ?>
	<fieldset>
		<legend><?php echo __('Add Challenge Answer'); ?></legend>
	<?php
		echo $this->Form->input('challenge_id');
		echo $this->Form->input('challenge_question_id');
		echo $this->Form->input('response');
		echo $this->Form->input('correct');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('controller' => 'challenge_questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('controller' => 'challenge_questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
