
 <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img class="images" src="img/banner1v3.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <img class="images" src="img/temp_logo.png" alt="First slide">
              <p class="vocabFont">We utilize tools and technologies every IT Professional uses daily, and through 
interactive examples and procedures build a world that begins to grow on its own:
driven by the developer. This creates a journey that participants can utilize 
throughout their career.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="img/banner2.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1 class="vocabFont">Who is Selected</h1>
             <p class="vocabFont">High School students can apply to the program and upon review and approval will have the opportunity of a lifetime.
Instead of a course or training material we are offering complete interaction from
the participants. We let the students choose there own path, and utilize the technologies
that interest them.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
<!--        <div class="item">
          <img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>-->
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->  
<div class="container homepage">
<!--<div class="jumbotron">
        <h1>A Developer's Journey</h1>
        <p class="lead">is a project focused on finding potential in bright minds. H4H thinks
that any young person should have the opportunity to see the adventure a career in
Information Technology can provide.</p>
        <p><a class="btn btn-lg btn-success" href="/users/apply" role="button">Apply today</a></p>
</div>-->
  <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-6">
          <img class="images" src="img/fishing.png" alt="Fish in a circle">
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-6">
		<br><br>
          <h1 class="vocabFont2">"Give a man a fish and you'll feed him for a day. Teach a man to fish and you feed him for a lifetime."</h1>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="vocabFont4">Hacking for Hope: Teaching kids to fish <span class="vocabFont5">...completely metaphorically</span></h2>
          <p class="lead">The  Bureau of Labor statistics projects that the job market for web developers will grow 20% faster than the average field over the next two decades. Meanwhile, reports have found that young people are three times as likely to be unemployed. For young men and women striving to overcome their current economic situations, a still-recovering American economy further narrows the pathway to a stable long-term employment outlook.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image" src="img/fishing2.png" alt="Man fishing in a boat">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img class="featurette-image" src="img/fishing3.png" alt="Fish jumping out of a computer">
        </div>
        <div class="col-md-7">
          <h2 class="vocabFont4">And we're talking about fishing <span class="vocabFont5">...not phishing</span></h2>
          <p class="lead">Hacking for Hope uses an entertaining and rewarding system of objectives and rewards to motivate young people as they progress from one level to the next. Participants move along a carefully designed pathway from complete novice to an intermediate developer able to take advantage of a hungry marketplace. Additionally, we provide the necessary ethical foundation to ensure that our participants becoming positive members of the online community. </p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="vocabFont4">It's all about hope<span class="vocabFont5"> ...enough about fish</span></h2>
          <p class="lead">Our country's poorest neighborhoods are lacking a lot of things: safety, security, adequate housing,   and access to nutritious food. However, the hardest thing to find is hope. The hope that it is possible to rise higher in life when all you see on an a day to day basis is other people fall. The hope that the promises of the American dream are for you. Learning a skill like web development will not only provide future employment, but something more valuable:  hope born from seeing yourself overcome challenges to create something truly your own.</p>
        </div>
        <div class="col-md-5"><br><br>
          <img class="featurette-image" src="img/fishing4.png" alt="An ichthys with Hope inside it">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->
<div class="row marketing">
        <div class="col-lg-6">
          <h4 class="vocabFont6">How it Works</h4>
          <p class="lead">We utilize tools and technologies every IT Professional uses daily, and through 
interactive examples and procedures build a world that begins to grow on its on:
driven by the developer. This creates a journey that participants can utilize 
throughout their career.</p>
<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
        </div>
        <div class="col-lg-6">
          <h4 class="vocabFont6">Who is Selected</h4>
          <p class="lead">High School students can apply to the program and, upon review and approval, will have the opportunity of a lifetime.
Instead of a course or training material, we are offering complete interaction from
the participants. We let the students choose there own path, and utilize the technologies
that interest them.</p>
<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
        </div>
</div>
</div>
