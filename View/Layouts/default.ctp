<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('HopeHacking', 'Hacking for Hope');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?php
        
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap');
                echo $this->Html->css('carousel');
                echo $this->Html->css('h4h');
		echo $this->Html->css('fontstylesheet');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
        
</head>
<body>
<!--      <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"></a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Sign up</a></li>
                
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>-->
			
   
	<div class="container">
		
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		
	</div>
<!--    <div id="footer">
			<?php // echo $this->Html->link(
					//$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					//'http://www.cakephp.org/',
					//array('target' => '_blank', 'escape' => false)
				//);
			?>
		</div>-->
	<?php // echo $this->element('sql_dump'); 
        
                echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js');
		echo $this->Html->script('bootstrap');
                echo $this->Html->script('docs.min');
                echo $this->Html->script('//tinymce.cachefly.net/4.0/tinymce.min.js');
                echo $this->Html->scriptBlock(
                    'tinymce.init({
                        selector: "textarea"
                     });'
                        );
                echo $this->Js->writeBuffer();
        ?>
</body>
</html>
   