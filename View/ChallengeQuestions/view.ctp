<div class="challengeQuestions view">
<h2><?php echo __('Challenge Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($challengeQuestion['ChallengeQuestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Challenge'); ?></dt>
		<dd>
			<?php echo $this->Html->link($challengeQuestion['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $challengeQuestion['Challenge']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($challengeQuestion['ChallengeQuestion']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($challengeQuestion['ChallengeQuestion']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Level Id'); ?></dt>
		<dd>
			<?php echo h($challengeQuestion['ChallengeQuestion']['level_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Challenge Question'), array('action' => 'edit', $challengeQuestion['ChallengeQuestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Challenge Question'), array('action' => 'delete', $challengeQuestion['ChallengeQuestion']['id']), null, __('Are you sure you want to delete # %s?', $challengeQuestion['ChallengeQuestion']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('controller' => 'challenge_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Challenge Answers'); ?></h3>
	<?php if (!empty($challengeQuestion['ChallengeAnswer'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Challenge Id'); ?></th>
		<th><?php echo __('Challenge Question Id'); ?></th>
		<th><?php echo __('Response'); ?></th>
		<th><?php echo __('Correct'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($challengeQuestion['ChallengeAnswer'] as $challengeAnswer): ?>
		<tr>
			<td><?php echo $challengeAnswer['id']; ?></td>
			<td><?php echo $challengeAnswer['challenge_id']; ?></td>
			<td><?php echo $challengeAnswer['challenge_question_id']; ?></td>
			<td><?php echo $challengeAnswer['response']; ?></td>
			<td><?php echo $challengeAnswer['correct']; ?></td>
			<td><?php echo $challengeAnswer['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'challenge_answers', 'action' => 'view', $challengeAnswer['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'challenge_answers', 'action' => 'edit', $challengeAnswer['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'challenge_answers', 'action' => 'delete', $challengeAnswer['id']), null, __('Are you sure you want to delete # %s?', $challengeAnswer['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
