<div class="challengeQuestions form">
<?php echo $this->Form->create('ChallengeQuestion'); ?>
	<fieldset>
		<legend><?php echo __('Edit Challenge Question'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('challenge_id');
		echo $this->Form->input('question');
		echo $this->Form->input('type');
		echo $this->Form->input('level_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ChallengeQuestion.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ChallengeQuestion.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Challenge Questions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('controller' => 'challenge_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
