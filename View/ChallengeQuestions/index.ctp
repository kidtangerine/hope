<div class="challengeQuestions index">
	<h2><?php echo __('Challenge Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('challenge_id'); ?></th>
			<th><?php echo $this->Paginator->sort('question'); ?></th>
			<th><?php echo $this->Paginator->sort('type'); ?></th>
			<th><?php echo $this->Paginator->sort('level_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($challengeQuestions as $challengeQuestion): ?>
	<tr>
		<td><?php echo h($challengeQuestion['ChallengeQuestion']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($challengeQuestion['Challenge']['name'], array('controller' => 'challenges', 'action' => 'view', $challengeQuestion['Challenge']['id'])); ?>
		</td>
		<td><?php echo h($challengeQuestion['ChallengeQuestion']['question']); ?>&nbsp;</td>
		<td><?php echo h($challengeQuestion['ChallengeQuestion']['type']); ?>&nbsp;</td>
		<td><?php echo h($challengeQuestion['ChallengeQuestion']['level_id']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $challengeQuestion['ChallengeQuestion']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $challengeQuestion['ChallengeQuestion']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $challengeQuestion['ChallengeQuestion']['id']), null, __('Are you sure you want to delete # %s?', $challengeQuestion['ChallengeQuestion']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Challenge Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Challenges'), array('controller' => 'challenges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge'), array('controller' => 'challenges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Challenge Answers'), array('controller' => 'challenge_answers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Challenge Answer'), array('controller' => 'challenge_answers', 'action' => 'add')); ?> </li>
	</ul>
</div>
