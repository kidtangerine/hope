<?php
App::uses('AppModel', 'Model');
/**
 * Challenge Model
 *
 * @property ChallengeAnswer $ChallengeAnswer
 * @property ChallengeDescription $ChallengeDescription
 * @property ChallengeQuestion $ChallengeQuestion
 */
class Challenge extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChallengeAnswer' => array(
			'className' => 'ChallengeAnswer',
			'foreignKey' => 'challenge_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ChallengeDescription' => array(
			'className' => 'ChallengeDescription',
			'foreignKey' => 'challenge_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ChallengeQuestion' => array(
			'className' => 'ChallengeQuestion',
			'foreignKey' => 'challenge_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
