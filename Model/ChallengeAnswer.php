<?php
App::uses('AppModel', 'Model');
/**
 * ChallengeAnswer Model
 *
 * @property Challenge $Challenge
 * @property ChallengeQuestion $ChallengeQuestion
 * @property User $User
 */
class ChallengeAnswer extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Challenge' => array(
			'className' => 'Challenge',
			'foreignKey' => 'challenge_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ChallengeQuestion' => array(
			'className' => 'ChallengeQuestion',
			'foreignKey' => 'challenge_question_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
