<?php
App::uses('AppModel', 'Model');
/**
 * ChallengeQuestion Model
 *
 * @property Challenge $Challenge
 * @property Level $Level
 * @property ChallengeAnswer $ChallengeAnswer
 */
class ChallengeQuestion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'question';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Challenge' => array(
			'className' => 'Challenge',
			'foreignKey' => 'challenge_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
//		'Level' => array(
//			'className' => 'Level',
//			'foreignKey' => 'level_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ChallengeAnswer' => array(
			'className' => 'ChallengeAnswer',
			'foreignKey' => 'challenge_question_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
